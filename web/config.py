"""
TODO: write mod string
"""

class BaseConfig():
    """
    Base configuration settings
    """
    DEBUG = True


class DevelopmentConfig(BaseConfig):
	"""
    Development configuration settings
    """
	DEBUG = True


class TestingConfig(BaseConfig):
	"""
    Testing configuration settings
    """
	DEBUG = True
	TESTING = True


class ProductionConfig(BaseConfig):
	"""
    Production configuration settings
    """
	DEBUG = False
