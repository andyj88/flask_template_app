from flask import render_template, Blueprint

# values accessable by all pages
main_blueprint = Blueprint("main", __name__, template_folder="templates")

# VIEWS: handlers that respond to requests from browsers.
# Flask handlers are written as functions (each view function is mapped to one or more request URLs)

@main_blueprint.route("/", methods=["GET", "POST"])
def homepage():
    '''
    Homepage view - function takes a template filename and a variable list of template args and returns the rendered template
    '''
    pg_name = "Homepage"
    return render_template("homepage.html", pg_name=pg_name)
