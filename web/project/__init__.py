from flask import Flask
from project.main.views import main_blueprint

app = Flask(__name__)
app.config.from_object("config.DevelopmentConfig")  # read and use config file

# Register blueprint
app.register_blueprint(main_blueprint)
